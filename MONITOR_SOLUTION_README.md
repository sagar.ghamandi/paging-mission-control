# Satellite Telemetry Monitor
This project processes satellite telemetry data, detecting anomalies in battery and thermostat readings to ensure the safety and functionality of the satellite's instruments.

## Overview
Satellite instruments are sensitive to environmental changes, especially temperature fluctuations. This project offers a monitoring solution that ingests telemetry data from satellites to detect any undesirable changes in the battery voltage and thermostat readings. When anomalies are detected, appropriate alerts are generated to notify the operational team.

### Usage
Place the telemetry data file (e.g., Sample_Data.txt) in the main directory.
Execute the program: python Monitor.py
The program will display alerts, if any, in JSON format.

#### Libraries Used
datetime: Handles time operations related to satellite readings.
json: Formats the output as a JSON.

##### Runtime
Python 3.8+