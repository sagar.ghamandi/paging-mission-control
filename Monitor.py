from datetime import datetime, timedelta
import json

def ingest_telemetry_data(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()
    
    readings = []
    for line in lines:
        split_line = line.strip().split('|')
        readings.append({
            'timestamp': datetime.strptime(split_line[0], "%Y%m%d %H:%M:%S.%f"),
            'satellite-id': int(split_line[1]),
            'red-high-limit': float(split_line[2]),
            'yellow-high-limit': float(split_line[3]),
            'yellow-low-limit': float(split_line[4]),
            'red-low-limit': float(split_line[5]),
            'raw-value': float(split_line[6]),
            'component': split_line[7]
        })
    return readings

def generate_alerts(readings):
    alerts = []
    cache = {}

    for reading in readings:
        satellite_id = reading['satellite-id']
        component = reading['component']
        timestamp = reading['timestamp']

        # Create keys for satellite and component if not present in cache
        if satellite_id not in cache:
            cache[satellite_id] = {}
        if component not in cache[satellite_id]:
            cache[satellite_id][component] = []

        cache[satellite_id][component].append(reading)
        cache[satellite_id][component] = [r for r in cache[satellite_id][component] if r['timestamp'] >= timestamp - timedelta(minutes=5)]

        if component == 'TSTAT' and len([r for r in cache[satellite_id][component] if r['raw-value'] > r['red-high-limit']]) >= 3:
            alerts.append({
                "satelliteId": satellite_id,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": timestamp.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            })

        elif component == 'BATT' and len([r for r in cache[satellite_id][component] if r['raw-value'] < r['red-low-limit']]) >= 3:
            alerts.append({
                "satelliteId": satellite_id,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": timestamp.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            })

    return alerts

def main():
    readings = ingest_telemetry_data('sample_data.txt')
    alerts = generate_alerts(readings)
    print(json.dumps(alerts, indent=4))

if __name__ == "__main__":
    main()
